<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/bulma.css">
    <title>Exemple PHP</title>
</head>
<body>

<?php

$user = [
    [ "nom" => "Warlock", "prenom" => "Oxzane", "comment" => "c'est hat les tableaux"],
    [ "nom" => "Valygar", "prenom" => "Maddy", "comment" => "c'est het les tableaux"],
    [ "nom" => "Athkins", "prenom" => "Torie", "comment" => "c'est hit les tableaux"],
    [ "nom" => "EngeO", "prenom" => "Shady", "comment" => "c'est hot les tableaux"],
    [ "nom" => "Willer", "prenom" => "Midajah", "comment" => "c'est hut les tableaux"],
    [ "nom" => "Lyhrel", "prenom" => "Floppy", "comment" => "c'est hyt les tableaux"],
];
?>
<div class="container is-fluid">
    <form action="/" method="GET" >
        <input class="input is-medium" type="text" name="prenom"  placeholder="Recherche"/><br />
        <input type="submit" value="Soumettre"/>
    </form>
</div>

<hr>
<section class="hero">
  <div class="hero-body">
    <div class="container is-fluid">
        <form action="/" method="POST" >
            <input class="input is-medium" type="text" name="prenom"  placeholder="Prénom"/><br />
            <input class="input is-medium" type="text" name="nom" placeholder="Nom" /><br />
            <textarea class="input" name="comment" id="comment" placeholder="Entrez votre commentaire ici!" cols="30" rows="10"></textarea>
            <br/>
            <div class="control">
                <input type="submit" Value="Commenter" class="button is-primary">
            </div>
        </form>
    </div>
  </div>
</section>
<hr>

<section class="hero">
  <div class="hero-body">
    <div class="container is-fluid">
        <h1 class="title">Commentaires</h1>
    </div>
  </div>
</section>

<section>
    <div class="container is-fluid">
    <?php
        foreach($user as $utilisateur) {
            //dans la boucle foreach, $user c'est le tableau alors que $utilisateur c'est chaque ligne
            //donc si on voulait les clé,s il s'agit de chaque $key de la ligne $utilisateur
    ?>
        <h5 class="subtitle is-5">Nom complet: <?php echo $utilisateur["prenom"] . " " . $utilisateur["nom"]; ?> </h5>
        <h6 class="subtitle is-6">Commentaire: </h6>
        <section>
        <?php echo $utilisateur["comment"]; ?>
        </section>
    </div>
</section>
<?php
        }
?>

<a href=index.php>Page 1</a>
</body>
</html>