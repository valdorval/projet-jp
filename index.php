<?php
    include_once './includes/parts/header.php';
$user = [
    [ "nom" => "Warlock", "prenom" => "Oxzane", "comment" => "c'est hat les tableaux"],
    [ "nom" => "Valygar", "prenom" => "Maddy", "comment" => "c'est het les tableaux"],
    [ "nom" => "Athkins", "prenom" => "Torie", "comment" => "c'est hit les tableaux"],
    [ "nom" => "EngeO", "prenom" => "Shady", "comment" => "c'est hot les tableaux"],
    [ "nom" => "Willer", "prenom" => "Midajah", "comment" => "c'est hut les tableaux"],
    [ "nom" => "Lyhrel", "prenom" => "Floppy", "comment" => "c'est hyt les tableaux"],
];
$variable = "Comments en anglais";
    include_once './includes/search-header.php';
    include_once './includes/comment/form-comment.php';
    /*require_once ou require : "require" va faire une erreur donc va arreter le code
    include_once  ou include : "include" va faire un warning  donc va quand meme continuer à exécuter le code
    la différence entre normal et _once c'est que si le fichier a déja été inclus ou requis, il ne va pas etre réinclus il va se servir du même
    en gros c'est comme quand on va chercher les fichiers js et css en html, sauf que la c'est pour ajouter du php*/
?>
<section class="hero">
  <div class="hero-body">
    <div class="container is-fluid">
        <h1 class="title"><?php echo $variable; ?></h1>
    </div>
  </div>
</section>
<section>
    <div class="container is-fluid">
    <?php
        for ($i = 0; $i < count($user); $i++) {
    ?>
        <h5 class="subtitle is-5">Nom complet: <?php echo $user[$i]["prenom"] . " " . $user[$i]["nom"]; ?> </h5>
        <h6 class="subtitle is-6">Commentaire: </h6>
        <section>
        <?php echo $user[$i]["comment"]; ?>
        </section>
    </div>
</section>
<?php
        }
?>
<?php
    include_once './includes/parts/footer.php';
?>

