<?php
    include_once './includes/functions/contact-functions.php';
    include_once './includes/parts/header.php';
    include_once './includes/search-header.php';
    if(isset($_POST["nom"])) {
        $nom = $_POST["nom"];
        echo "Merci d'avoir soumis vos commentaires";
    }

?>

<section>
    <h1 class="title is-1">Contactez-nous!</h1>
    <div class="container is-fluid">
        <form action="/contact.php" method="POST">
            <p>
                <input placeholder="Nom" type="text" name="nom" value="<?php form_values("nom") ?>">
            </p>
           <p>
                <input placeholder="Prénom" type="text" name="prenom" value="<?php form_values("prenom") ?>">
           </p>
            <p>
                <input placeholder="Courriel" type="email" name="courriel" value="<?php form_values("courriel") ?>">
            </p>
             <p>
                <textarea name="contact" placeholder="Votre message" id="contact" cols="30" rows="10"> <?php form_values("contact") ?></textarea>
              </p>
              <input type="submit" value="Soumettre votre demande">
        </form>
    </div>
</section>

<?php
    include_once './includes/parts/footer.php';